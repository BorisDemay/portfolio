import React, {useState} from 'react';
import Navbar from './Navbar';
import Body from './Body';

import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'

library.add(fas, far, fab)

function App() {
    document.title = "Boris Demay - Portfolio";

    const [isDarkMode, setIsDarkMode] = useState(false);

    function handleMode() {
        setIsDarkMode(!isDarkMode)
    }

    return (
        <div className={`${isDarkMode ? 'text-primary-color bg-[#1D1D20]' : 'text-secondary-color bg-white'} h-screen font-body`}>
            <Navbar isDarkMode={isDarkMode} handleMode={handleMode}/>
            <Body/>
        </div>
    );
}

export default App;
