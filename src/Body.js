import React from 'react';

export default function Body() {
    return (
        <main>
            <body>
            <section className="flex tracking-tight text-3xl md:text-5xl w-4/5 m-auto">
                <div className="m-auto">
                    <p className="">
                        I'm French web developer.<br/><br/>
                        I work mainly on Laravel / React, I also enjoy working with other more innovative technologies.
                    </p>
                </div>
            </section>
            </body>
        </main>
    )
}