import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

function Navbar(props) {
    const {handleMode} = props;
    const {isDarkMode} = props;

    return (
        <header>
            <nav
                className="flex flex-wrap items-center justify-between mx-auto pt-10 py-4 md:pt-4 px-4 md:px-20 text-lg">
                <a className="text-lg lg:text-4xl" href="/">Boris Demay</a>

                <ul className="flex justify-between pt-0 text-lg lg:text-4xl space-x-3">
                    <li className="md:p-4 py-2 block">
                        <a href="/">
                            <FontAwesomeIcon icon="fa-regular fa-envelope"/>
                        </a>
                    </li>
                    <li className="md:p-4 py-2 block">
                        <a href="/">
                            <FontAwesomeIcon icon="fa-brands fa-gitlab"/>
                        </a>
                    </li>
                    <li className="md:p-4 py-2 block">
                        <a href="/">
                            <FontAwesomeIcon icon="fa-brands fa-github"/>
                        </a>
                    </li>
                    {/*<li className="md:p-4 py-2 block">
                        <button onClick={handleMode}>
                            <FontAwesomeIcon icon={`${isDarkMode ? 'fa-regular fa-sun' : 'fa-regular fa-moon'}`}/>
                        </button>
                    </li>*/}
                    <li className="md:p-4 py-2 block">
                        <label className="relative inline-flex items-center cursor-pointer">
                            <input type="checkbox" value="" className="sr-only peer" onClick={handleMode}/>
                            <div
                                className="w-14 h-7 bg-gray-200 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full peer-checked:after:border-white after:absolute after:top-0.5 after:left-[4px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-6 after:w-6 after:transition-all peer-checked:bg-blue-600">
                            </div>
                            <span
                                className="ml-3 text-sm">{`${isDarkMode ? 'Dark-Mode' : 'Light-Mode'}`}</span>

                        </label>
                    </li>
                </ul>
            </nav>
        </header>
    );
}

export default Navbar;
