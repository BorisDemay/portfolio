/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors')

module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        "primary-color": "var(--white-color)",
        "secondary-color": "var(--black-color)",
        'Cinereous' : '#92817A',
        'AntiqueRuby' : '#93032E',
        'CGBlue' : '#1282A2',
      },
      fontFamily: {
        body: ['Sono', 'sans-serif'],
      },
    },
  },
  plugins: [],
}

